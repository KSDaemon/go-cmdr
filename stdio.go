// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package cmdr

import (
	"context"
	"io"
	"os"
)

type stdioKey struct{}

type stdioValue struct {
	stdin  io.Reader
	stdout io.Writer
	stderr io.Writer
}

// WithStdio returns a context remembering the standard IO streams.
//
// Tests can create a context which provides a set of bytes.Buffer objects as
// IO streams, avoiding the need to maintain global variables which are
// modified during test execution.
func WithStdio(parent context.Context, stdin io.Reader, stdout, stderr io.Writer) context.Context {
	return context.WithValue(parent, stdioKey{}, stdioValue{stdin, stdout, stderr})
}

// Stdio returns the standard IO streams for the given context.
//
// If the context was returned by WithStdio, the encoded streams are recovered.
// In all other cases os.{Stdin,Stdout,Stderr} is returned.
//
// IO streams may be replaced for testing. Recovering the streams from the
// context avoids the need to use and manage per-command global variables.
func Stdio(ctx context.Context) (stdin io.Reader, stdout, stderr io.Writer) {
	if streams, ok := ctx.Value(stdioKey{}).(stdioValue); ok {
		return streams.stdin, streams.stdout, streams.stderr
	}

	return os.Stdin, os.Stdout, os.Stderr
}
