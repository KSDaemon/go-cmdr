// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package cmdr_test

import (
	"context"
	"errors"
	"reflect"
	"testing"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/cmdtest"
)

func TestRunNonFailingCommand(t *testing.T) {
	cmd := cmdr.Func(func(ctx context.Context, args []string) error {
		return nil
	})

	cmdr.RunMain(cmd, []string{})
}

func TestRunFailingCommand(t *testing.T) {
	cmd := cmdr.Func(func(ctx context.Context, args []string) error {
		return errors.New("I cannot do that Dave")
	})

	inv := cmdtest.Invoke(cmd)

	if err := inv.ExpectExitCode(1); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStdout(""); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStderr("go-cmdr error: I cannot do that Dave\n"); err != nil {
		t.Fatal(err)
	}
}

type messageError string

func (e messageError) Error() string {
	return string(e)
}

func (e messageError) ExitMessage() string {
	return string(e)
}

func TestRunCommandWithExitMessenger(t *testing.T) {
	cmd := cmdr.Func(func(ctx context.Context, args []string) error {
		return messageError("I cannot do that Dave")
	})

	inv := cmdtest.Invoke(cmd)

	if err := inv.ExpectExitCode(1); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStdout(""); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStderr("I cannot do that Dave\n"); err != nil {
		t.Fatal(err)
	}
}

func TestRunCommandWithSilentError(t *testing.T) {
	cmd := cmdr.Func(func(ctx context.Context, args []string) error {
		return cmdr.SilentError(42)
	})

	inv := cmdtest.Invoke(cmd)

	if err := inv.ExpectExitCode(42); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStdout(""); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStderr(""); err != nil {
		t.Fatal(err)
	}
}

func TestPassingArguments(t *testing.T) {
	cmd := cmdr.Func(func(ctx context.Context, args []string) error {
		if !reflect.DeepEqual(args, []string{"first", "second"}) {
			t.Fatalf("Unexpected arguments passed to command function: %q", args)
		}

		return nil
	})

	inv := cmdtest.Invoke(cmd, "first", "second")

	if err := inv.ExpectExitCode(0); err != nil {
		t.Fatal(err)
	}

	// Using WithARgv0 doesn't force observability of Argv0 on the command function.
	cmdr.RunMain(cmdr.Func(func(ctx context.Context, args []string) error {
		if !reflect.DeepEqual(args, []string{"first", "second"}) {
			t.Fatalf("Unexpected arguments passed to command function: %q", args)
		}

		return nil
	}), []string{"prog" + cmdr.ExeSuffix, "first", "second"}, cmdr.WithArgv0("prog.bin"))
}

// Func0 is like cmdr.Func but observes the program name argument.
type Func0 func(ctx context.Context, args []string) error

func (fn Func0) Run(ctx context.Context, args []string) error {
	return fn(ctx, args)
}

func (fn Func0) PreserveArgs0() bool {
	return true
}

// Ensure that Func0 implements cmdr.Args0Observer
var _ cmdr.Args0Observer = Func0(nil)

func TestPassingArgv0(t *testing.T) {
	cmd := Func0(func(ctx context.Context, args []string) error {
		if !reflect.DeepEqual(args, []string{"go-cmdr", "first", "second"}) {
			t.Fatalf("Unexpected arguments passed to command function: %q", args)
		}

		return nil
	})

	inv := cmdtest.Invoke(cmd, "first", "second")

	if err := inv.ExpectExitCode(0); err != nil {
		t.Fatal(err)
	}

	// Using WithArgv0 does not modify passed arguments. The option only affects
	// the message printed in case of errors.
	cmdr.RunMain(Func0(func(ctx context.Context, args []string) error {
		if !reflect.DeepEqual(args, []string{"prog" + cmdr.ExeSuffix, "first", "second"}) {
			t.Fatalf("Unexpected arguments passed to command function: %q", args)
		}

		return nil
	}), []string{"prog" + cmdr.ExeSuffix, "first", "second"}, cmdr.WithArgv0("prog.bin"))
}

func TestCmdName(t *testing.T) {
	cmd := cmdr.Func(func(ctx context.Context, args []string) error {
		if v := cmdr.Name(ctx); v != "foo" {
			t.Fatalf("Unexpected command name: %v", v)
		}

		return nil
	})

	inv := cmdtest.InvokeWithArgv0(cmd, "foo")

	if err := inv.ExpectExitCode(0); err != nil {
		t.Fatal(err)
	}
}

func TestRunSubCommand(t *testing.T) {
	cmd := cmdr.Func(func(ctx context.Context, args []string) error {
		if v := cmdr.Name(ctx); v != "cmd" {
			t.Fatalf("Unexpected command name: %v", v)
		}

		if len(args) > 0 && args[0] == "sub" {
			subCmd := cmdr.Func(func(ctx context.Context, args []string) error {
				if v := cmdr.Name(ctx); v != "cmd sub" {
					t.Fatalf("Unexpected command name: %v", v)
				}

				return nil
			})

			return cmdr.RunSubCommand(ctx, subCmd, args[0], args[1:])
		}

		return nil
	})

	inv := cmdtest.InvokeWithArgv0(cmd, "cmd", "sub")

	if err := inv.ExpectExitCode(0); err != nil {
		t.Fatal(err)
	}
}
