// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package cmdr_test

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/cmdtest"
)

type helloCmd struct{}

func (helloCmd) Run(ctx context.Context, args []string) error {
	fmt.Println("Hello World")
	return nil
}

// You can structure your application as a number of command types that get
// invoked at the right time. All you need is a `Run(context.Context, []string)
// error` function on your types. Inside that function you can do anything.
func ExampleRunMain() {
	cmdr.RunMain(helloCmd{}, []string{"example"})
	// Output: Hello World
}

// The most basic example uses `cmdr.RunMain` to run a function that takes a
// context, arguments and returns an error. The context has multiple uses as we
// will see later. The `cmdr.Func` type adapts a function to `cmdr.Cmd`
// interface. In essence anything that can be called with string arguments is
// runnable as a command.
func ExampleFunc() {
	cmd := cmdr.Func(func(ctx context.Context, args []string) error {
		fmt.Println("Hello World")

		return nil
	})

	cmdr.RunMain(cmd, []string{"example"})
	// Output: Hello World
}

// Apart from serving the usual role, the context may be used to obtain the
// three standard input and output streams. The returned streams are os.Stdin,
// os.Stdout and os.Stderr. Un unit tests those may be redirected to buffers to
// aid in testing the output produced by commands. This is especially useful and
// easy with the cmdtest package.
func ExampleStdio() {
	cmd := cmdr.Func(func(ctx context.Context, args []string) error {
		_, stdout, _ := cmdr.Stdio(ctx)

		_, _ = fmt.Fprintln(stdout, "Hello World")

		return nil
	})
	inv := cmdtest.Invoke(cmd)

	if err := inv.ExpectStdout("Hello World\n"); err != nil {
		log.Fatal(err)
	}
	// This produces no output as the output is captured the invocation helper
	// and is available for tests to inspect.

	// Output:
}
