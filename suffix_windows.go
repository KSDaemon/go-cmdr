// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

//go:build windows

package cmdr

// ExeSuffix is the suffix for executable programs.
const ExeSuffix = ".exe"
