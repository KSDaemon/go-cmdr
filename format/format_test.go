// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package format_test

import (
	"errors"
	"io"
	"testing"

	"gitlab.com/zygoon/go-cmdr/format"
)

func TestOutputFormat_Unknown(t *testing.T) {
	t.Setenv("OUTPUT_FORMAT", "potato")

	_, err := format.Negotiate(io.Discard, nil)

	if err == nil {
		t.Fatal("Unexpected success with OUTPUT_FORMAT=potato")
	}

	if !errors.Is(err, format.ErrUnknownFormat) {
		t.Fatalf("Expected ErrUnknownFormat error, got %v", err)
	}

	if v := err.Error(); v != "unknown output format" {
		t.Fatalf("Unexpected error message: %q", v)
	}

	if v := err.(interface{ ExitCode() int }).ExitCode(); v != 65 {
		t.Fatalf("Unexpected exit code: %d", v)
	}
}

type customSupport struct {
	priority int
}

type customPrinter struct {
	w        io.Writer
	priority int // To correlate with customSupport.priority.
}

func (customSupport) FormatName() string {
	return "custom"
}

func (sup customSupport) FormatPrinter(w io.Writer, _, _ []string) any {
	return customPrinter{w: w, priority: sup.priority}
}

func (sup customSupport) FormatPriority([]string) int {
	return sup.priority
}

func TestOutputFormat_Priorities(t *testing.T) {
	t.Setenv("OUTPUT_FORMAT", "custom")

	t.Run("order 0, -1, +1", func(t *testing.T) {
		someP, err := format.Negotiate(io.Discard, nil, customSupport{}, customSupport{priority: -1}, customSupport{priority: 1})
		if err != nil {
			t.Fatal(err)
		}

		p := someP.(customPrinter)
		if p.priority != 1 {
			t.Fatalf("High-priority support not selected")
		}
	})

	t.Run("order -1, 0, +1", func(t *testing.T) {
		someP, err := format.Negotiate(io.Discard, nil, customSupport{priority: -1}, customSupport{}, customSupport{priority: 1})
		if err != nil {
			t.Fatal(err)
		}

		p := someP.(customPrinter)
		if p.priority != 1 {
			t.Fatalf("High-priority support not selected")
		}
	})

	t.Run("order -1, +1, 0", func(t *testing.T) {
		someP, err := format.Negotiate(io.Discard, nil, customSupport{priority: -1}, customSupport{priority: 1}, customSupport{})
		if err != nil {
			t.Fatal(err)
		}

		p := someP.(customPrinter)
		if p.priority != 1 {
			t.Fatalf("High-priority support not selected")
		}
	})
}
