// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package format

import (
	"fmt"
	"io"
	"os"
)

// PlainTextSupport indicates application support for plain-text output.
var PlainTextSupport Support = &formatSupport{
	printer: func(w io.Writer, _, _ []string) any {
		return &PlainPrinter{w: w, NoColor: os.Getenv("NO_COLOR") != ""}
	},
}

// PlainPrinter assists in printing plain text.
type PlainPrinter struct {
	w io.Writer

	// NoColor implements the https://no-color.org/ quasi-standard.
	//
	// When NO_COLOR is a non-empty value, the application should refrain
	// from using ANSI escape codes that make the output more colorful.
	NoColor bool
}

// Printf prints formatted output.
func (p *PlainPrinter) Printf(format string, a ...any) (n int, err error) {
	// TODO: we could filter out format sequences related to color changes here.
	return fmt.Fprintf(p.w, format, a...)
}

// Println prints values followed by newline.
func (p *PlainPrinter) Println(a ...any) (n int, err error) {
	// TODO: we could filter out format sequences related to color changes here.
	return fmt.Fprintln(p.w, a...)
}
