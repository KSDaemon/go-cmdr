// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

// Package jsonformat implements support for outputting JSON. It is separated
// from the format package to avoid pulling in encoding/json and reflect into
// all format-aware applications.
//
// Call format.Negotiate with jsonformat.Support and type-switch on
// the return value to obtain jsonformat.OutputHelper.
package jsonformat

import (
	"encoding/json"
	"io"

	"gitlab.com/zygoon/go-cmdr/format"
)

// Support indicates application support for JSON output.
//
// The "pretty" property controls JSON pretty-printing. By default, the format
// is designed for machines for parse, producing minimal, valid output.
var Support format.Support = support{}

type support struct{}

func (support) FormatName() string {
	return "json"
}

func (support) FormatPrinter(w io.Writer, _, props []string) any {
	p := Printer{enc: json.NewEncoder(w)}

	for _, prop := range props {
		if prop == "pretty" {
			p.enc.SetIndent("", "  ")
		}
	}

	return &p
}

// Printer assists in producing valid JSON output.
type Printer struct {
	enc *json.Encoder
}

// PrintEncoded prints the JSON encoding of an object.
func (h *Printer) PrintEncoded(v any) error {
	return h.enc.Encode(v)
}
