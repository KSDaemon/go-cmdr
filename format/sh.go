// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package format

import (
	"fmt"
	"io"
	"strings"
)

// ShellCompatSupport indicates application support for shell-compatible output.
var ShellCompatSupport Support = &formatSupport{
	name: "sh",
	printer: func(w io.Writer, _, props []string) any {
		return &ShellPrinter{w: w}
	},
}

// ShellPrinter assists in producing shell-compatible output.
type ShellPrinter struct {
	w io.Writer
}

// PrintComment prints a shell comment.
func (p *ShellPrinter) PrintComment(text string) error {
	_, err := fmt.Fprintf(p.w, "# %s\n", strings.ReplaceAll(text, "\n", "\n# "))

	return err
}

// PrintSetVar prints a variable assignment.
func (p *ShellPrinter) PrintSetVar(name, value string) error {
	_, err := fmt.Fprintf(p.w, "%s=%q\n", name, value)

	return err
}

// PrintExportVar prints an exported variable assignment.
func (p *ShellPrinter) PrintExportVar(name, value string) error {
	_, err := fmt.Fprintf(p.w, "export %s=%q\n", name, value)

	return err
}
