// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

// Package cmdtest contains utilities for testing commands.
package cmdtest

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/internal/diff"
)

const (
	msgExpected = "(expected)"
	msgActual   = "( actual )"
)

// Invocation represents a single execution of a command for testing.
type Invocation struct {
	Cmd                   cmdr.Cmd
	Args                  []string
	Stdin, Stdout, Stderr bytes.Buffer
	ExitCode              int
}

// Invoke returns an Invocation for the given command and arguments.
//
// The tested command will be invoked with argv0 matching the name of the
// package being tested. Use InvokeWithArgv0 to provide a custom value.
//
// Tested command is passed to cmdr.RunMain for execution. Instead of exiting,
// non-zero exit code is recorded for inspection. Standard input-output
// streams are replaced by byte buffers.
//
// Note that IO redirection does not happen at the OS level. Tested commands
// must use cmdr.Stdio helper function to access suggested IO interfaces.
func Invoke(cmd cmdr.Cmd, args ...string) *Invocation {
	if len(os.Args) == 0 {
		panic("os.Args must have at least one argument")
	}

	argv0 := filepath.Base(os.Args[0])
	argv0 = strings.TrimSuffix(strings.TrimSuffix(argv0, cmdr.ExeSuffix), ".test")

	return InvokeWithArgv0(cmd, argv0, args...)
}

// InvokeWithArgv0 is like Invoke with explicit argv0.
func InvokeWithArgv0(cmd cmdr.Cmd, argv0 string, args ...string) *Invocation {
	a := make([]string, 0, len(args)+1)
	a = append(a, argv0)
	a = append(a, args...)
	inv := &Invocation{Cmd: cmd, Args: a}

	cmdr.RunMain(inv.Cmd, inv.Args,
		cmdr.WithArgv0(argv0),
		cmdr.WithExitFunc(func(code int) { inv.ExitCode = code }),
		cmdr.WithPanicBehavior(cmdr.RecoverTraceExit),
		cmdr.WithBaseContext(
			cmdr.WithStdio(context.Background(), &inv.Stdin, &inv.Stdout, &inv.Stderr)))

	return inv
}

// ExpectExitCode checks an expected exit code.
func (inv *Invocation) ExpectExitCode(code int) error {
	if v := inv.ExitCode; v != code {
		return fmt.Errorf("unexpected exit code: %d", v)
	}

	return nil
}

// ExpectStdout assets presence of expected content printed to stdout.
func (inv *Invocation) ExpectStdout(c CannedOutput) error {
	if a, e, ok := c.MatchesBuffer(&inv.Stdout); !ok {
		u, err := diff.ToUnified(msgExpected, msgActual, e, diff.Strings(e, a))
		if err != nil {
			return err
		}

		return fmt.Errorf("unexpected stdout content\n%s", u)
	}

	return nil
}

// ExpectStderr assets presence of expected content printed to stderr.
func (inv *Invocation) ExpectStderr(c CannedOutput) error {
	if a, e, ok := c.MatchesBuffer(&inv.Stderr); !ok {
		u, err := diff.ToUnified(msgExpected, msgActual, e, diff.Strings(e, a))
		if err != nil {
			return err
		}

		return fmt.Errorf("unexpected stderr content\n%s", u)
	}

	return nil
}

// CannedOutput represents expected output of a command.
//
// This type automatically accepts conversions from string literals but makes
// it harder to misuse with variables of string type.
type CannedOutput string

// MatchesBuffer returns actual, expected strings and equality flag
//
// The contents of the buffer is transformed, such, so that white-space
// differences become more apparent. Tabs present in the buffer are replaced
// with <TAB>, while any line-trailing white-space is terminated with <EOL>.
func (c CannedOutput) MatchesBuffer(buf *bytes.Buffer) (a, e string, ok bool) {
	if bytes.ContainsRune([]byte(c), '\t') {
		panic("CannedOutput must not contain tab characters, use <TAB> to represent them.")
	}

	if bytes.Contains([]byte(c), []byte{' ', '\n'}) {
		panic("CannedOutput must not contain trailing spaces, use <EOL> to disambiguate them.")
	}

	tmp := buf.Bytes()
	// This avoids confusion related to tabs and spaces.
	tmp = bytes.ReplaceAll(tmp, []byte("\t"), []byte("<TAB>"))
	// This avoids confusion related to trailing spaces.
	tmp = bytes.ReplaceAll(tmp, []byte(" \n"), []byte(" <EOL>\n"))

	a = string(tmp)
	e = string(c)

	return a, e, a == e
}
