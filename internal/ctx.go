// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package internal

import (
	"context"
)

type nameKey struct{}

// WithName returns a context remembering the command name.
func WithName(parent context.Context, name string) context.Context {
	return context.WithValue(parent, nameKey{}, name)
}

// Name returns the name of currently executing command.
func Name(ctx context.Context) (name string) {
	if name, ok := ctx.Value(nameKey{}).(string); ok {
		return name
	}

	panic("context misuse, command name not set")
}
