// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package router_test

import (
	"context"
	"flag"
	"reflect"
	"testing"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/cmdtest"
	"gitlab.com/zygoon/go-cmdr/router"
)

func runManually(inv *cmdtest.Invocation) {
	cmdr.RunMain(inv.Cmd, inv.Args,
		cmdr.WithExitFunc(func(code int) { inv.ExitCode = code }),
		cmdr.WithBaseContext(cmdr.WithStdio(context.Background(), &inv.Stdin, &inv.Stdout, &inv.Stderr)))
}

func TestSmoke(t *testing.T) {
	inv := cmdtest.Invoke(&router.Cmd{})

	if err := inv.ExpectExitCode(1); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStdout(""); err != nil {
		t.Fatal(err)
	}

	// The word "router" comes from the package name.
	if err := inv.ExpectStderr(`Usage: router COMMAND [...]
router error: expected command name
`); err != nil {
		t.Fatal(err)
	}
}

func TestExplicitRouting(t *testing.T) {
	ch := make(chan struct{}, 1)

	r := router.Cmd{
		Commands: map[string]cmdr.Cmd{
			"cmd-1": cmdr.Func(func(ctx context.Context, args []string) error {
				ch <- struct{}{}

				if v := cmdr.Name(ctx); v != "router cmd-1" {
					t.Fatalf("Unexpected command name: %v", v)
				}

				if !reflect.DeepEqual(args, []string{"cmd-2"}) {
					t.Fatalf("Unexpected arguments passed to command function: %q", args)
				}

				return nil
			}),
			"cmd-2": cmdr.Func(func(ctx context.Context, args []string) error {
				t.Fatalf("Unexpected call to command function")

				return nil
			}),
		},
	}

	inv := cmdtest.Invoke(&r, "cmd-1", "cmd-2")

	if _, ok := <-ch; !ok {
		t.Fatalf("Command function was not called")
	}

	if err := inv.ExpectExitCode(0); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStdout(""); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStderr(""); err != nil {
		t.Fatal(err)
	}
}

func TestImplicitRouting(t *testing.T) {
	ch := make(chan struct{}, 1)

	r := router.Cmd{
		Implicit: cmdr.Func(func(ctx context.Context, args []string) error {
			ch <- struct{}{}

			if v := cmdr.Name(ctx); v != "router" {
				t.Fatalf("Unexpected command name: %v", v)
			}

			if len(args) != 0 {
				t.Fatalf("Unexpected arguments passed to command function: %q", args)
			}

			return nil
		}),
		Commands: map[string]cmdr.Cmd{
			"cmd-1": cmdr.Func(func(ctx context.Context, args []string) error {
				if !reflect.DeepEqual(args, []string{"bar"}) {
					t.Fatalf("Unexpected arguments passed to command function: %q", args)
				}

				return nil
			}),
			"cmd-2": cmdr.Func(func(ctx context.Context, args []string) error {
				t.Fatalf("Unexpected call to command function")

				return nil
			}),
		},
	}

	inv := cmdtest.Invoke(&r)

	if _, ok := <-ch; !ok {
		t.Fatalf("Command function was not called")
	}

	if err := inv.ExpectExitCode(0); err != nil {
		t.Fatal(err)
	}
}

func TestArgv0Routing(t *testing.T) {
	cmd1Called := make(chan struct{}, 1)

	r := router.Cmd{
		UseArgv0: true,
		Commands: map[string]cmdr.Cmd{
			"cmd-1": cmdr.Func(func(ctx context.Context, args []string) error {
				cmd1Called <- struct{}{}

				if v := cmdr.Name(ctx); v != "cmd-1" {
					t.Fatalf("Unexpected command name: %v", v)
				}

				if !reflect.DeepEqual(args, []string{"arg"}) {
					t.Fatalf("Unexpected arguments passed to command function: %q", args)
				}

				return nil
			}),
			"cmd-2": cmdr.Func(func(ctx context.Context, args []string) error {
				t.Fatalf("Unexpected call to command function")

				return nil
			}),
		},
	}

	if !r.PreserveArgs0() {
		t.Fatalf("router did not request preservation of argv0")
	}

	inv := cmdtest.InvokeWithArgv0(&r, "cmd-1", "arg")

	if _, ok := <-cmd1Called; !ok {
		t.Fatalf("Command function was not called")
	}

	if err := inv.ExpectExitCode(0); err != nil {
		t.Fatal(err)
	}

	// Routing on argv0 automatically looks at the basename of args[0].
	// Executable suffix, if present is automatically stripped.
	inv = cmdtest.InvokeWithArgv0(&r, "/some/path/cmd-1"+cmdr.ExeSuffix, "arg")

	if _, ok := <-cmd1Called; !ok {
		t.Fatalf("Command function was not called")
	}

	if err := inv.ExpectExitCode(0); err != nil {
		t.Fatal(err)
	}
}

func TestArgv0RoutingWithoutAnyArgs(t *testing.T) {
	r := router.Cmd{
		UseArgv0: true,
		Commands: map[string]cmdr.Cmd{
			"cmd-1": cmdr.Func(func(ctx context.Context, args []string) error {
				return nil
			}),
			"cmd-2": cmdr.Func(func(ctx context.Context, args []string) error {
				return nil
			}),
		},
	}

	inv := &cmdtest.Invocation{Cmd: &r}
	runManually(inv) // Because we created Invocation by hand.

	if err := inv.ExpectExitCode(1); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStdout(""); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStderr(`Usage: ? COMMAND [...]

Available commands:
  cmd-1  <EOL>
  cmd-2  <EOL>
? error: expected command name
`); err != nil {
		t.Fatal(err)
	}
}

func TestMissingCmd(t *testing.T) {
	r := router.Cmd{
		Commands: map[string]cmdr.Cmd{
			"cmd-1": cmdr.Func(func(ctx context.Context, args []string) error {
				return nil
			}),
		},
	}

	inv := cmdtest.Invoke(&r)

	if err := inv.ExpectExitCode(1); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStdout(""); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStderr(`Usage: router COMMAND [...]

Available commands:
  cmd-1  <EOL>
router error: expected command name
`); err != nil {
		t.Fatal(err)
	}
}

func TestUnknownCmd(t *testing.T) {
	r := router.Cmd{
		Commands: map[string]cmdr.Cmd{
			"cmd-1": cmdr.Func(func(ctx context.Context, args []string) error {
				return nil
			}),
		},
	}

	inv := cmdtest.Invoke(&r, "cmd-2")

	if err := inv.ExpectExitCode(1); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStdout(""); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStderr(`Usage: router COMMAND [...]

Available commands:
  cmd-1  <EOL>
router error: unknown command
`); err != nil {
		t.Fatal(err)
	}
}

func TestNesting(t *testing.T) {
	cmd := &router.Cmd{
		Commands: map[string]cmdr.Cmd{
			"top": &router.Cmd{
				Commands: map[string]cmdr.Cmd{
					"sub": &router.Cmd{
						Commands: map[string]cmdr.Cmd{
							"bottom": cmdr.Func(func(ctx context.Context, args []string) error {
								_, _, stderr := cmdr.Stdio(ctx)
								fs := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError) // Can we know our name here?
								fs.SetOutput(stderr)

								return fs.Parse(args)
							}),
						},
					},
				},
			},
		},
	}

	inv := cmdtest.Invoke(cmd, "top", "sub", "bottom", "--help")

	if err := inv.ExpectStderr("Usage of router top sub bottom:\n"); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectExitCode(64); err != nil {
		t.Fatal(err)
	}
}

func TestCustomNamesAndNesting(t *testing.T) {
	cmd := &router.Cmd{
		Name: "custom-router",
		Commands: map[string]cmdr.Cmd{
			"top": &router.Cmd{
				Commands: map[string]cmdr.Cmd{
					"sub": &router.Cmd{
						Name: "custom-sub",
						Commands: map[string]cmdr.Cmd{
							"bottom": cmdr.Func(func(ctx context.Context, args []string) error {
								_, _, stderr := cmdr.Stdio(ctx)
								fs := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError) // Can we know our name here?
								fs.SetOutput(stderr)

								return fs.Parse(args)
							}),
						},
					},
				},
			},
		},
	}

	inv := cmdtest.Invoke(cmd, "top", "sub", "bottom", "--help")

	// The help message is confusing but allows for backwards compatibility and arbitrary
	// customizations in special situations. Note that "custom-router" and "top".
	if err := inv.ExpectStderr("Usage of custom-sub bottom:\n"); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectExitCode(64); err != nil {
		t.Fatal(err)
	}
}
