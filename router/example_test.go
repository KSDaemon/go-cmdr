// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package router_test

import (
	"context"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/router"
)

// You can arrange your commands into sub-command trees using the `router.Cmd`
// type. That type is just a runnable command that depending on the arguments
// invokes one of the named arguments, an implicit command (if any) or returns
// an error.
func ExampleCmd() {
	cmd := &router.Cmd{
		Commands: map[string]cmdr.Cmd{
			"hello": cmdr.Func(func(ctx context.Context, args []string) error {
				fmt.Println("Hello!")

				return nil
			}),
			"bye": cmdr.Func(func(ctx context.Context, args []string) error {
				fmt.Println("Good bye!")

				return nil
			}),
		},
	}

	cmdr.RunMain(cmd, []string{"example", "hello"})
	// Output: Hello!
}

// Implicit commands are useful for providing default behavior. They are invoked
// if the user does not provide a command to run.
func ExampleCmd_Implicit() {
	cmd := &router.Cmd{
		Implicit: cmdr.Func(func(ctx context.Context, args []string) error {
			fmt.Println("Implicit behavior")

			return nil
		}),
	}

	cmdr.RunMain(cmd, []string{"example"})
	// Output: Implicit behavior
}
