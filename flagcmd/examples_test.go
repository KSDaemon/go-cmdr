// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package flagcmd_test

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
)

type debugKeyType struct{}

// You can use MakeCmd to make a command out of a runner and any number of extenders.
func ExampleMakeCmd() {
	var debugKey debugKeyType

	debugEnabled := func(ctx context.Context) bool {
		return *ctx.Value(debugKey).(*bool)
	}
	debugExt := flagcmd.ExtensionFunc(func(ctx context.Context, fl *flag.FlagSet) context.Context {
		return context.WithValue(ctx, debugKey, fl.Bool("debug", false, "Enable debugging"))
	})
	base := flagcmd.RunnerFunc(func(ctx context.Context, fl *flag.FlagSet) error {
		if debugEnabled(ctx) {
			_, _ = fmt.Println("Debug enabled")
		}
		_, _ = fmt.Println("Hello World")

		return nil
	})
	cmd := flagcmd.MakeCmd(base, debugExt)
	cmdr.RunMain(cmd, []string{"example", "-debug"})
	// Output: Debug enabled
	// Hello World
}
