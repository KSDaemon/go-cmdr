// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package flagcmd

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"
)

// RunnerFunc adapts a Go function to act as a FlagRunner.
type RunnerFunc func(ctx context.Context, fl *flag.FlagSet) error

// RunFlag runs the underlying function.
func (fn RunnerFunc) RunFlag(ctx context.Context, fl *flag.FlagSet) error {
	return fn(ctx, fl)
}

// FlagSet returns a new, configured FlagSet.
//
// The name of the flag set is constructed using cmdr.Name.
func (RunnerFunc) FlagSet(ctx context.Context) *flag.FlagSet {
	return flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
}

// FlagUsage implements Helper.
//
// FlagUsage prints stock usage instructions and flag defaults.
func (RunnerFunc) FlagUsage(fl *flag.FlagSet) {
	_, _ = fmt.Fprintf(fl.Output(), "Usage: %s\n", fl.Name())

	fl.PrintDefaults()
}
