// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package cmdr

import (
	"context"
)

// Cmd is a runnable command.
type Cmd interface {
	// Run runs the command with the given context and arguments.
	// The context can be used to pass global options to a command.
	Run(ctx context.Context, args []string) error
}

// Func is the type of functions runnable as commands.
type Func func(ctx context.Context, args []string) error

// Run runs the underlying function.
func (fn Func) Run(ctx context.Context, args []string) error {
	return fn(ctx, args)
}
